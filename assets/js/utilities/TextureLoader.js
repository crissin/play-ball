
const TextureLoader = new THREE.TextureLoader()
  .setCrossOrigin('use-credentials') // does not work with wildcard CORS policy
export default TextureLoader;
