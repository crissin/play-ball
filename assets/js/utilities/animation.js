import { TweenMax } from 'gsap';

// total time, translated to speed, for the bat swing
const hitLength = 1.2;

// bat is pulled back and swung
export function batterUp(bat) {
  TweenMax.fromTo(bat.rotation, hitLength, {
    z: Math.PI / -4.8
  }, {
    z: Math.PI / 1.8,
    ease: Power4.easeIn
  });

  TweenMax.fromTo(bat.position, hitLength, {
    x: -50,
    y: 0,
    z: -50
  }, {
    x: -170,
    y: 100,
    z: 0,
    ease: Power4.easeIn
  });

  TweenMax.fromTo(bat.rotation, hitLength, {
    y: 0
  }, {
    y: Math.PI / 1,
    ease: Power4.easeIn,
    onComplete: () => {
      followThrough(bat);
    }
  });
}

// after the ball is hit, the bat follows through with the swing
function followThrough(bat) {
  TweenMax.to(bat.rotation, 0.4, {
    z: Math.PI / 2.6,
    ease: Power4.easeOut
  });
  TweenMax.to(bat.position, 0.4, {
    x: -170,
    y: 60,
    z: -60,
    ease: Power4.easeOut
  });
  TweenMax.to(bat.rotation, 0.9, {
    y: Math.PI / 1,
    ease: Power4.easeOut
  });
}

// baseball is pitched
export function thePitch(ball, pos) {
  TweenMax.to(ball.rotation, 0.9, {
    y: 100,
    ease: Power4.easeOut
  });
  TweenMax.fromTo(ball.position, hitLength, {
    x: pos.x,
    y: pos.y,
    z: pos.z
  }, {
    x: 100,
    y: 100,
    z: 0,
    ease: Power4.easeIn,
    onComplete: () => {
      homeRun(ball);
    }
  })
}

// baseball is hit
function homeRun(ball) {
  TweenMax.to(ball.rotation, 0.9, {
    x: '-= 82',
    z: '+= 80',
    ease: Power4.easeOut
  });
  TweenMax.fromTo(ball.position, 3, {
    x: 100,
    y: 100,
    z: 0
  }, {
    x: -200,
    y: 390,
    z: -900,
    ease: Power4.easeOut
  })
}
