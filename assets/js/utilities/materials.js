import TextureLoader from './TextureLoader.js';
// [TEXTURES]

// baseball bat texture
const batTexture = TextureLoader.load('assets/images/PC_Baseball_Bat_13_BaseColor.jpg');
// baseball bat normal
const batNormal = TextureLoader.load('assets/images/PC_Baseball_Bat_13_Normal.jpg');
// baseball texture
const baseballTexture = TextureLoader.load('assets/images/baseball_ball_01_c.jpg');
// baseball normal
const baseballNormal = TextureLoader.load('assets/images/baseball_ball_01_n.jpg');
// ground texture
const dirt = TextureLoader.load('assets/images/dirty.jpg', function(dirt) {
  dirt.wrapS = dirt.wrapT = THREE.RepeatWrapping;
  dirt.offset.set(3, 3);
  dirt.repeat.set(12, 12);
});
// ground normal
const dirtNormal = TextureLoader.load('assets/images/dirty-normal.jpg', function(dirt) {
  dirt.wrapS = dirt.wrapT = THREE.RepeatWrapping;
  dirt.offset.set(3, 3);
  dirt.repeat.set(12, 12);
});
// home plate
const baseTexture = TextureLoader.load('assets/images/home-plate.jpg');

// enviornment maps
const enviornment = new THREE.CubeTextureLoader()
  .setPath('assets/images/env/')
  .load(['px.png', 'nx.png', 'py.png', 'ny.png', 'pz.png', 'nz.png']);


// [MATERIALS]

const batMaterial = new THREE.MeshPhongMaterial({
  map: batTexture,
  normalMap: batNormal,
  normalScale: new THREE.Vector2(5, 5),
  color: 'rgb(255, 255, 255)',
  reflectivity: 0.25,
  envMap: enviornment
});

const baseballMaterial = new THREE.MeshPhongMaterial({
  map: baseballTexture,
  normalMap: baseballNormal,
  normalScale: new THREE.Vector2(2, 2),
  color: 'rgb(255, 255, 255)',
  reflectivity: 0.25,
  envMap: enviornment
});

const groundMaterial = new THREE.MeshPhongMaterial({
  map: dirt,
  normalMap: dirtNormal,
  normalScale: new THREE.Vector2(7, 7)
});

const baseMaterial = new THREE.MeshPhongMaterial({
  map: baseTexture
});

export const materialList = [batMaterial, baseballMaterial, groundMaterial, baseMaterial]
