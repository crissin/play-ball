
export default function(scene ) {
  const ambient = new THREE.HemisphereLight(0xc5b798, 0xffffff, .5);
  ambient.position.set(0, 200, 0);
  scene.add(ambient);

  const light = new THREE.DirectionalLight(0xffffff, .8);
  light.position.set(0, 200, 100);
  light.castShadow = true;
  light.shadow.camera.top = 180;
  light.shadow.camera.bottom = -100;
  light.shadow.camera.left = -120;
  light.shadow.camera.right = 120;
  scene.add(light);
}
