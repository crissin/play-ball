export default (url) => (
  fetch(url, {
    credentials: 'include'
  }).then(response => response.json())
)
