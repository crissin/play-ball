import DataFetcher from './utilities/DataFetcher.js';
import addLights from './utilities/addLights';
import { batterUp, thePitch } from './utilities/animation';
// [MATERIALS]
import { materialList } from './utilities/materials';

let controls;
let camera, scene, renderer;
let composer;
// lets to container models
let homeBase;
let baseBall;
let baseBallBat;
// array to store loaded meshes
let loadedModels = [];
// json loader
const loader = new THREE.JSONLoader();
// y coord of ground floor
const groundLevel = -60;
// pitched base balls initial position
const baseBallInitial = new THREE.Vector3(0, 100, -900);


function init() {
  // container to hold canvas
  const container = document.getElementById('container');
  // camera created and set
  camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000);
  camera.position.set(100, 200, 300);
  // scene and atmosphere created and set
  scene = new THREE.Scene();
  scene.background = new THREE.Color(0xa0a0a0);
  scene.fog = new THREE.Fog(0xa0a0a0, 200, 1000);
  // lights initialized
  addLights(scene);
  // controls initialized
  controls = new THREE.OrbitControls( camera );
  controls.enableZoom = false;
  controls.enablePan = false;
  controls.enableDamping = true;
  controls.dampingFactor = .16;
  controls.maxPolarAngle = Math.PI / 2.5;
  controls.target.set( 0, 100, 0 );

  // ground properties set
  const ground = new THREE.Mesh(new THREE.PlaneBufferGeometry(4000, 4000, 300), materialList[2]);
  ground.rotation.x = -Math.PI / 2;
  ground.position.y = groundLevel;
  ground.castShadow = true;
  ground.receiveShadow = true;

  // baseball properties set
  baseBall = loadedModels[0].clone();
  baseBall.position.copy(baseBallInitial);
  baseBall.scale.set(45, 45, 45);

  // stray ball created and positioned
  const strayBall = loadedModels[0].clone();
  strayBall.position.set(100, -50, -70);
  strayBall.scale.set(45, 45, 45);

  // baseball bat properties set
  baseBallBat = loadedModels[1];
  baseBallBat.scale.set(350, 350, 350);
  baseBallBat.rotateZ( Math.PI / -4.8);
  baseBallBat.position.set(-50, 0, -50);

  // baseball home plate properties set
  homeBase = loadedModels[2];
  homeBase.rotation.x = -Math.PI / 2;
  homeBase.rotation.z = -Math.PI / 1;
  homeBase.scale.set(4.5, 4.5, 4.5);
  homeBase.position.set(0, groundLevel, -150);

  // models added to scene
  scene.add(ground, baseBall, strayBall, baseBallBat, homeBase);

  // renderer initialized and properties set
  renderer = new THREE.WebGLRenderer({
    alpha: true,
    antialias: true
  });

  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.shadowMap.enabled = true;
  renderer.autoClear = false;

  container.appendChild(renderer.domElement);

  window.addEventListener('resize', onWindowResize, false);
}

// click event listener to trigger animation
document.querySelector('.hit').addEventListener('click', () => {
  thePitch(baseBall, baseBallInitial);
  batterUp(baseBallBat);
})

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
  requestAnimationFrame(animate);
  controls.update();
  render();
}

function render() {
  renderer.render(scene, camera);
}

// promises to return relevant models
function geometryLoader({
  dataUrl,
  materialIndex,
  modelName
}) {
  return new Promise((resolve, reject) => {

    loader.load(dataUrl, (loadedGeometry) => {
      const mesh = new THREE.Mesh(loadedGeometry, materialList[materialIndex]);
      mesh.name = modelName;
      mesh.castShadow = true;
      mesh.receiveShadow = true;
      // This mesh payload is what is added to the object's group
      resolve(mesh);
    });
  });
}

// [ STEP 2. ] The function call
// [NOTE] Kick off the rendering with JUST the models data
function playBall({
  models
}) {
  // Iterate over the models array and return out a promise for each group of models
  const themePromises = models.map(({
    data
  }) => {
    const geometries = data.geometries; // variablize the geometries data
    const group = new THREE.Object3D(); // Create a group to house all the respective geometries
    const groupName = data.name;
    group.name = groupName;
    // Iterate over the variable quantity of geometries per theme object
    const geometryLoaders = geometries.map(({
      data,
      materialIndex,
    }, index) => (
      // Return out a promise to load each geometry file and the THREE Mesh
      geometryLoader({
        dataUrl: `data:application/json,${encodeURI(JSON.stringify(data))}`,
        modelName: `${groupName}-${materialIndex}`,
        materialIndex: data.materialIndex,
      })
    ));
    // Iterate through all the loader promises and add each mesh to the model group
    return Promise.all(geometryLoaders).then((meshes) => {
      group.add(...meshes);
      // Return the group from each Promise so they can be assigned to a global
      return group;
    });
  });

  Promise.all(themePromises).then((modelGroups) => {
    // Assign the global models array to the returned modelGroups so it can be referenced in the
    // addModel selection process
    loadedModels = modelGroups;
    init();
    animate();
  });
}

// [ STEP 1. ] Get the data from the JSON
DataFetcher('./assets/data/modal-data.json')
  .then(response => {
    const themeData = response[0];
    // [ STEP 2. ] Call the playBall function to kick everything off
    playBall(themeData);
  });
