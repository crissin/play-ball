var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('default', function() {
  console.log('play ball');
});

gulp.task('sass', function() {
  return gulp.src('./assets/css/**/*.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('./assets/css'))
});

gulp.task('watch', function(){
  gulp.watch('./assets/css/**/*.scss', ['sass']);
  // Other watchers
})
