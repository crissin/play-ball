#Play Ball

The objective of this project was to demonstrate and solve the problem of creating a 3d scene with an animated baseball bat.

In order to solve this objective the webGL library, Three.js, was utilized along side GSAPS animation library, TweenMax.  Both these libraries were chosen due to their strong foundation, stability and flexibility. These qualities are always critical when integrating into any repo, in order to ensure scalability. The project is abstracted out into its basic and primary components, with DRYNESS kept in mind. An example of this scalability, is the promises and compiled .json data file, which easily returns out geometrical data and its relevant materials composited into a mesh, should additional models be integrated.

If I were to spend additional time on the project, I would integrate a realtime physics engine such as Cannon.js or Ammo.js. This would allow real world physics and response in regards to the animation. I would also allow a method for the user to control the rate of force that is applied to the bat and thus the ball (maybe even shattering the bat at a certain force and speed).


## Getting Started

In order to initialize this project, simply download the repo and run ‘npm install’ to get dependencies, and ’npm run start’ within the directory. Use 'npm run build' to generate static assets.


## Built With

- Three.js
- GSAP - TweenMax

## Authors

**Chris Gaither**

## Demo
http://chriswakeup.com/play-ball/
